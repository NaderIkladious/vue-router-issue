Vue.http.options.root = '/api/v1';
Vue.config.debug = true;

var App = new Vue({
  el: '#app'
})

Vue.filter('int', function(value) {
  return parseInt(value);
})

var router = new VueRouter({
	history: false
})

router.map({
  '/': {
		component: {
      template: '<h1>test 1</h1>'
    }
	},
  '/another': {
		component: {
      template: '<h1>test 2</h1>'

    }
	}
})

router.start(App, '#app')
